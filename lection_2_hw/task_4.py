# Вывести на печать букву “Z” в таком виде:
# *******
#       *
#      *
#     *
#    *
#   *
#  *******


def letter_z():
    width = 7
    height = 8
    print('*' * width)
    for i in range(height - 2):
        print(' ' * (width - i - 1) + '*')
    print('*' * width)


letter_z()