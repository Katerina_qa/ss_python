# Напишите программу для преобразования температуры в градусы Цельсия и
# Фаренгейта и обратно.
# Примеры входных данных и ожидаемых результатов:
# 60С -> 60C is 140F
# 45F -> 45F is 7C

def temperature_conversion(temp):
    degree = int(temp[:-1])
    m_unit = temp[-1]

    if m_unit.upper() == "C":
        result = int(round((9 * degree) / 5 + 32))
        unit = "F"
    elif m_unit.upper() == "F":
        result = int(round((degree - 32) * 5 / 9))
        unit = "C"
    else:
        print("Input proper convention.")
        quit()

    print(temp, "is", str(result) + unit)


temperature_conversion("60F")
