# Пользователь вводить 6 карт из списка доступных:
# 2, 3, 4, 5, 6, 7, 8, 9,10, 'J', 'Q', 'K', 'A'.
# У каждой карты есть свой "вес":
# 2, 3, 4, 5, 6 весят +1
# 7, 8, 9 весят 0
# 10, 'J', 'Q', 'K', 'A' весят -1
# Задача: имея список карт, введенных пользователем посчитать их
# общий “вес” и вывести результат на печать.
# Примеры входных данных и ожидаемых результатов:
# 2, 3, 4, 10, 'Q', 5 -> общий вес = 2
# 'A', 3, 4, 10, 'J', 4 -> общий вес = 0
# 2, 7, 4, 9, 3, 5 -> общий вес = 4


def cards_count():
    list_k = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    list_v = [1, 1, 1, 1, 1, 0, 0, 0, -1, -1, -1, -1, -1]
    d_cards = dict(zip(list_k, list_v))
    print(d_cards)

    input_data = input(
        "Please input 6 cards from listed above to count a total weight: ")
    input_list = input_data.split(', ')
    if len(input_list) < 6 or len(input_list) > 6:
        print("Please input exactly 6 cards from mentioned.")
    else:
        res = 0
        for i in input_list:
            if i in d_cards:
                res += d_cards[i]
        print("total weight = " + str(res))


cards_count()
